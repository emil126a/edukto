<?php

return [
   
    'address' => 'Bizim ünvan',
    'street' => 'Tbilisi prospekti 97',
    'contact_us'=>'Bizimlə əlaqə',
    'name'=>'Adiniz',
    'email'=>'Email',
    'message'=>'Mesajınız',
    'send'=>'Göndər',
    'click_to_maximize'=>'Şəkli böyütmək üçün tıklayın',
    'gallery'=>'Qalereya',
    'home'=>'Əsas səhifə',
    'lang_bridge'=>'Xarici dil sizə körpülər tikir',
    'have_question'=>'Sualınız var?',
    'offer_solution'=>'Sizi maraqlandıran istənilən məsələ ilə bağlı bizimlə əlaqə saxlaya bilərsiniz.',
    'english_lessons'=>'İngilis dili dərsləri',
    'english_lessons_desc'=>' Biz hər bir tələbəmiz üçün bağlanğıc mərhələdən professional mərhələyə kimi İngilis dili kursları təşkil edirik.',
    'education_abroad'=>'Xaricdə təhsil',
    'education_abroad_desc'=>'Nüfuzlu universitetlərdə təhsil almaq istəyirsinisə bizə qoşulun. Biz sizə hər prosesdə dəstək verəcəyik.',
    'our_prof_lectures'=>'Bizim peşəkar müəllimlərimiz',
    'teachers'=>'Müəllimlər',
    'about_teacher'=>'Müəllim haqqında',
    



];
