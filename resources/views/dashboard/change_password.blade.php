@extends('layouts.app')

@section('content')
@section('guest_page_url', url('/'))
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Şifrəni yenilə</div>
                <div class="panel-body">

  

@if(Session::has('message')) 

  <div class="alert alert-success" role="alert">
     
    <strong>Diqqət!</strong> {{{Session::get('message')}}}
  </div>

@endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/change_password') }}">
                        {{ csrf_field() }}


                         <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Yeni Şifrə</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Yeni Şifrə təkrarı</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                 <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                     Şifrəni yenilə
                                </button>
 
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection