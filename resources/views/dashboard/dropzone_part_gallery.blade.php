 

@section('bottom')

<script type="text/javascript" src='{{asset("theme/admin/dropzone/dropzone.js")}}'></script>
 <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js'></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript">
 
 
function sort_items()
{
var sort_order_result=new Array();


 $('.sortable').sortable(
{

   cursor: 'move', 
        tolerance: 'pointer', 
        revert: true,
    forcePlaceholderSize: true,
    beforeStop: function( event, ui ) {
      $('#add_image').html('');
      sort_order_result=new Array();
    },
    stop: function(evt, ui) {
            $.ajax({
            type: 'POST',
            headers: {
              'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          url: "{{url($folder.'_images_sort')}}",
          data: {
            sort_array: sort_order_result,
           
          },
 
          success: function(file){
              
 
 
 
              //console.log(degrees);
            
            }
          });
        },

 
    update: function(event, ui){
      $('.sortable ').find('.dz-preview').each(
        function(){

          $('#add_image').append('<input type="text" name="sort_order_{{$folder}}_images[] " value="'+$(this).find('img').attr('alt')+'">');

          sort_order_result.push($(this).find('img').attr('alt'));


           
        }

 
        )
 console.log(sort_order_result);

    }
}


  );
}


$( document ).ready(function() {










 $('body').on('click', '.rotate_right', function(){
 
  // $(this).parents().find('span').attr('degree','45445');
/*  alert('clicked');*/

$(this).parent().find('.degree').attr('degree');
    var image_file_name=$(this).parents('.dz-preview').children('.dz-image').find('img').attr('alt');

    var degrees=parseInt($(this).parent().find('.degree').attr('degree'));

/*    console.log(degrees);*/

  var image=$(this).parents('.dz-preview').children('.dz-image').find('img');

$('.rotate_right').addClass('not-active');

 degrees +=90;


$(this).parent().find('.degree').attr('degree',degrees);
//$(this).attr('degree',degrees);

     $.ajax({
            type: 'POST',
            headers: {
              'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          url: "{{url('rotate_'.$folder.'_image')}}",
          data: {
            filename: image_file_name,
          degree:-90,
          folder:"{{$folder}}"
          },
 
          success: function(file){
              
image.css({
 
      'transform': 'rotate(' + degrees + 'deg)',
      '-ms-transform': 'rotate(' + degrees + 'deg)',
      '-moz-transform': 'rotate(' + degrees + 'deg)',
      '-webkit-transform': 'rotate(' + degrees + 'deg)',
      '-o-transform': 'rotate(' + degrees + 'deg)'
    });

$('.rotate_right').removeClass('not-active');
 
              //console.log(degrees);
            
            }
          });
 
   });

$('body').on('click', '.rotate_left', function(){
 
  // $(this).parents().find('span').attr('degree','45445');
/*  alert('left clicked');*/

$(this).parent().find('.degree').attr('degree');
    var image_file_name=$(this).parents('.dz-preview').children('.dz-image').find('img').attr('alt');

    var degrees=parseInt($(this).parent().find('.degree').attr('degree'));

/*    console.log(degrees);*/

  var image=$(this).parents('.dz-preview').children('.dz-image').find('img');

$('.rotate_left').addClass('not-active');

 degrees -=90;


$(this).parent().find('.degree').attr('degree',degrees);
//$(this).attr('degree',degrees);

     $.ajax({
            type: 'POST',
            headers: {
              'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          url: "{{url('rotate_'.$folder.'_image')}}",
          data: {
            filename: image_file_name,
          degree:90,
          folder:"{{$folder}}"
          },
 
          success: function(file){
              
image.css({
 
      'transform': 'rotate(' + degrees + 'deg)',
      '-ms-transform': 'rotate(' + degrees + 'deg)',
      '-moz-transform': 'rotate(' + degrees + 'deg)',
      '-webkit-transform': 'rotate(' + degrees + 'deg)',
      '-o-transform': 'rotate(' + degrees + 'deg)'
    });

$('.rotate_left').removeClass('not-active');
 
              //console.log(degrees);
            
            }
          });
 
   });


  sort_items();
   //	var degrees = 0;  
    //rotate($('.rotate_right'),90,-90);
 

});



  Dropzone.options.myAwesomeDropzone = {
 /* paramName: "file", // The name that will be used to transfer the file*/
/*  maxFilesize:0.5, // MB*/
  paramName: "file", // The name that will be used to transfer the file
/*  addRemoveLinks: true,*/
/*  acceptedFiles: ".jpeg,.jpg,.png,.gif",*/
  init: function() {

 
 /*Dropzone images before hand start*/

 @if(count($stored_images)>0)
        var myDropzone = this;
 

        var mockFile = [
        @foreach($stored_images as $key=>$image)
      
        {
          name: "{{$image->name}}",
          size: {{Storage::size('public/'.$folder.'/'.$image->name)}},
          accepted: true,
          kind: 'image',
          upload: {
            filename: '{{$image}}',
          },
          dataURL:"{{asset('storage/'.$folder.'/'.$image->name)}}",

        },


        @endforeach


        

      ];

/*console.log(mockFile);*/

    for (let i = 0; i < mockFile.length; i++) {

      myDropzone.files.push(mockFile[i]);
      myDropzone.emit("addedfile", mockFile[i]);
      myDropzone.createThumbnailFromUrl(
        mockFile[i],
        myDropzone.options.thumbnailWidth,
        myDropzone.options.thumbnailHeight,
        myDropzone.options.thumbnailMethod,
        true,
        function(thumbnail) {
          myDropzone.emit('thumbnail', mockFile[i], thumbnail);
          myDropzone.emit("complete", mockFile[i]);
        }
      ); 
    }
      
@endif




 /*Dropzone images before hand end*/

    this.on("error", function(file, response) {
                // do stuff here.
              
        var error_message=response.errors.file[0];
        
        
        $(file.previewElement).find('.dz-error-message').html('<span style="word-wrap: break-word;">'+error_message+'</span>');
        
 
              });

    this.on("success", function(file, responseText) {
         
         file.upload.filename=responseText;
          $(file.previewElement).children('.dz-image').find('img').attr('alt',responseText);
    $( document ).ready(function() {
     /*     rotate($('.rotate_right'),90,-90);
          rotate($('.rotate_left'),-90,90);
          sort_items();*/
    })
   


        $(file.previewElement).find('[data-dz-name]').text(responseText);




            $('<input>').attr('type','text').attr('name','{{$folder}}_images[]').attr('value',responseText).appendTo('#add_image');
    
    });


    this.on("removedfile", function(file) {
        
       var image_file_name=$(file.previewElement).find('[data-dz-name]').text();
       $(':input[value="'+image_file_name+'"]').remove();
 
       $.ajax({
            type: 'POST',
            headers: {
              'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          url: "{{url('delete_'.$folder.'_image')}}",
          data: {
          	filename: image_file_name,
          	folder:"{{$folder}}"
          },
 
          success: function(file){
              
            }
          });
    })

  }
}


 
</script>
@endsection