@extends('layouts.app')
@section('guest_page_url', url('/teachers'))
 
@section('head')
<style type="text/css">
  .click
  {
    cursor: pointer;
  }

  .click:hover {
    background-color: #babdc1 !important;
  }
</style>

@endsection



@section('content')

            
 

 
<div class="container">
  <div class="row">
    <div class="col-md-11" style="margin: 0 auto; margin-left: 4%;">
      <div class="panel panel-default">
        <div class="panel-heading">Müəllimlər</div>

        <div class="panel-body">
          @if(Session::has('message')) 
<br style="clear: both;"><br style="clear: both;">
  <div class="alert alert-success" role="alert">
     
    <strong>Diqqət!</strong> {{{Session::get('message')}}}
  </div>

@endif
<a style="float:right;" href="{{url(App::getLocale().'/admin/add_teacher')}}"><button class="btn btn-lg btn-success" type="button"><i class="icon-pencil"></i> <i class="fa fa-plus"></i> ƏLAVƏ ET</button></a>
           <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Şəkil</th>
      <th scope="col">Kurs</th>
      <th scope="col">Müəllim</th>

      <th scope="col" style="width:200px;"></th>
    </tr>
  </thead>
  <tbody>
  @foreach($teachers as $teacher)
 
  <tr class="click">
      <td scope="row">
 

        <img class="img-responsive" style="width: 120px" src="{{asset('storage/team/'.$teacher->image)}}" alt="team">


     </td>
      <td>{{$teacher->course_az}}</td>
      <td>{{$teacher->fullname_az}}</td>

      <td align="right">
      <a href="{{url('admin/edit_teacher/'.$teacher->id)}}" class="btn btn-primary">Düzəliş et</a>
      <a href="{{url('admin/delete_teacher/'.$teacher->id)}}" class="btn btn-danger delete">Sil</a>
      </td>
    </tr>
    <tr style="display: none;" class="toggle">
      <td colspan="5" scope="row">Məzmun: {!!$teacher->desc_az!!}</td>
     </tr>


  @endforeach

  </tbody>
</table>

<center>    {{ $teachers->links() }}</center>
       </div>
     </div>
   </div>
 </div>
</div>



















@endsection


@section('bottom')
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
     
   <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/js/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js'></script>
<script type="text/javascript">

$(document).ready(function() {

    
$(".fancybox").fancybox({
    // API options 
    autoScale: false,
    type: 'iframe',
    padding: 0,
    closeClick: false
});
} );


</script>
  {{ Html::script('theme/admin/lightbox/ekko-lightbox.min.js') }}
  <script type="text/javascript">
    
    $(document).ready(
      function()
      {
        $('.click').click(function()
          {
           $(this).closest('tr').next('.toggle').toggle('slow'); 
          });
        
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
      }
      );


  </script>

@endsection