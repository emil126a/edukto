@extends('layouts.app')
@section('guest_page_url', url('/homepage'))
@section('head')

{{ Html::style('theme/admin/lightbox/ekko-lightbox.css') }}
<style type="text/css">
  .click
  {
    cursor: pointer;
  }

  .click:hover {
    background-color: #babdc1 !important;
  }
</style>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/css/lightbox.css"> 
@endsection



@section('guest_page_url', url('/homepage'))

@section('content')

            
 

 
<div class="container">
  <div class="row">
    <div class="col-md-11" style="margin: 0 auto; margin-left: 4%;">
      <div class="panel panel-default">
        <div class="panel-heading">Səhifələr</div>

        <div class="panel-body">
          @if(Session::has('message')) 
<br style="clear: both;"><br style="clear: both;">
  <div class="alert alert-success" role="alert">
     
    <strong>Diqqət!</strong> {{{Session::get('message')}}}
  </div>

@endif
<a style="float:right;" href="{{url(App::getLocale().'/admin/add_content')}}"><button class="btn btn-lg btn-success" type="button"><i class="icon-pencil"></i> <i class="fa fa-plus"></i> ƏLAVƏ ET</button></a>
           <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Şəkil</th>
      <th scope="col">Başlıq</th>
      <th scope="col">Kateqoriya</th>
      <th scope="col">Yenilənmə tarixi</th>
      <th scope="col" style="width:200px;"></th>
    </tr>
  </thead>
  <tbody>
  @foreach($contents as $content)
 
  <tr class="click">
      <td scope="row">
@if (isset($content->images[0]))

         <a class="example-image-link" href="{{asset('storage/content_images/big_images/'.$content->images[0]['image'])}}" data-lightbox="{{$content->id}}" data-title="Click the right half of the image to move forward."><img class="example-image" src="{{asset('storage/content_images/thumbnail/'.$content->images[0]['image'])}}" alt="" width="100"/></a>
<?php
$b = false;
?>
      @foreach($content->images as $image)

      @if(!$b)


                        <?php

                          $b = true;
                          continue;
                        ?>
                      @endif
 <a class="example-image-link" style='display:none;' href="{{asset('storage/content_images/big_images/'.$image->image)}}" data-lightbox="{{$content->id}}" data-title="Or press the right arrow on your keyboard."></a>


      @endforeach

@endif

        


     </td>
      <td>{{$content->title_az}}</td>
      <td>{{$content->category->text_az}}</td>
      <td>{{$content->updated_at}}</td>
      <td align="right">
      <a href="{{url('admin/edit_content/'.$content->id)}}" class="btn btn-primary">Düzəliş et</a>
      <a href="{{url('admin/delete_content/'.$content->id)}}" class="btn btn-danger delete">Sil</a>
      </td>
    </tr>
    <tr style="display: none;" class="toggle">
      <td colspan="5" scope="row">Məzmun: {!!$content->desc_az!!}</td>
     </tr>


  @endforeach

  </tbody>
</table>

<center>    {{ $contents->links() }}</center>
       </div>
     </div>
   </div>
 </div>
</div>



















@endsection


@section('bottom')
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
     
   <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/js/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js'></script>
<script type="text/javascript">

$(document).ready(function() {

    
$(".fancybox").fancybox({
    // API options 
    autoScale: false,
    type: 'iframe',
    padding: 0,
    closeClick: false
});
} );


</script>
  {{ Html::script('theme/admin/lightbox/ekko-lightbox.min.js') }}
  <script type="text/javascript">
    
    $(document).ready(
      function()
      {
        $('.click').click(function()
          {
           $(this).closest('tr').next('.toggle').toggle('slow'); 
          });
        
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
      }
      );


  </script>

@endsection