@extends('layouts.app')
  @section('guest_page_url', url('/homepage'))
@section('head')
        <meta name="csrf-token" content="{{ csrf_token() }}">


        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('bs-iconpicker/css/bootstrap-iconpicker.min.css')}}">
@endsection
 
@section('content')
 





<!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>




<script src='{{asset("bs-iconpicker/jquery-menu-editor.js?v2")}}'></script>
<script src='{{asset("bs-iconpicker/js/iconset/iconset-fontawesome-4.2.0.min.js")}}'></script>
<script src='{{asset("bs-iconpicker/js/bootstrap-iconpicker.js?v2")}}'></script> -->





<div class="container">  


            <div class="row">
                <div class="col-md-12"><h2>Kateqoriyalar</h2></div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix"><h5 class="pull-left">Menyular</h5>
                             
                        </div><br>
                        <div id="successMessage" class="alert alert-success" style="width: 90%; margin: 0 auto; display: none;">
 
  <strong>Qeyd!</strong> Müvəffəqiyyətlə yadda saxlanıldı!
</div>
                        <div class="panel-body" id="cont">
                            <ul id="myList" class="sortableLists list-group">
                            </ul>
                        </div>
                    </div>
                    <div class="form-group" align="right">
                    <button id="btnOut" type="button" class="btn btn-primary"><i class="glyphicon glyphicon-sort"></i> Sıralanmanı yadda saxla</button>
                    </div>
                
                </div>
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Düzəliş / Əlavə et</div>
                        <div class="panel-body">
                            <form id="frmEdit" class="form-horizontal">
                                <input type="hidden" name="mnu_icon" id="mnu_icon">
                                <div class="form-group">
                                    <label for="mnu_text_az" class="col-sm-3 control-label">Başlıq (AZ)</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="mnu_text_az" name="mnu_text_az" placeholder="Azərbaycan dilində">
                                            <div class="input-group-btn">
                                                <button id="mnu_iconpicker" class="btn btn-default" data-iconset="fontawesome" data-icon="" type="button"></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             <div class="form-group">
                                    <label for="mnu_slug" class="col-sm-3 control-label">Title (EN)</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="mnu_text_en" name="mnu_text_en" placeholder="In English">
                                    </div>
                                </div>
 <div class="form-group">
                                    <label for="mnu_slug" class="col-sm-3 control-label">Название (RU)</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="mnu_text_ru" name="mnu_text_ru" placeholder="На русском языке">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="mnu_slug" class="col-sm-3 control-label">Slug</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="mnu_slug" name="mnu_slug" placeholder="Slug">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="mnu_target" class="col-sm-3 control-label">Target</label>
                                    <div class="col-sm-9">
                                        <select id="mnu_target" name="mnu_target" class="form-control">
                                            <option value="_self">Self</option>
                                            <option value="_blank">Blank</option>
                                            <option value="_top">Top</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    
                                        <!-- <input type="hidden" class="form-control" id="mnu_parent_id" name="mnu_parent_id" placeholder="Text"> -->
                                      <input type="hidden" class="form-control" id="mnu_id" name="mnu_id" placeholder="Text">
                                </div>
                               
                                
                            </form>
                        </div>
                        <div class="panel-footer">
                            <button type="button" id="btnUpdate" class="btn btn-primary" disabled><i class="fa fa-refresh"></i> Düzəliş et</button>
                            <button type="button" id="btnAdd" class="btn btn-success"><i class="fa fa-plus"></i> Əlavə et</button>
                        </div>
                    </div>
                    
                </div>
            </div>
            <hr>
        <footer>

        </footer>
        </div>


      

        @endsection



        @section('bottom')
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        
        <script src='{{asset("bs-iconpicker/jquery-menu-editor.js?v9")}}'></script>
        <script src='{{asset("bs-iconpicker/js/iconset/iconset-fontawesome-4.2.0.min.js")}}'></script>
        <script src='{{asset("bs-iconpicker/js/bootstrap-iconpicker.js?v1")}}'></script>
        <script>
            jQuery(document).ready(function () {
                var strjson = '{!!json_encode($categories)!!}';
                var iconPickerOpt = {cols: 5, searchText: "Axtar...", labelHeader: 'Cəmi:<b>{1}</b>. Göstərilir: {0}', footer: false};
                var options = {
                    hintCss: {'border': '1px dashed #13981D'},
                    placeholderCss: {'background-color': 'gray'},
                    opener: {
                        as: 'html',
                        close: '<i class="fa fa-minus"></i>',
                        open: '<i class="fa fa-plus"></i>',
                        openerCss: {'margin-right': '10px'},
                        openerClass: 'btn btn-success btn-xs'
                    }
                };
                var editor = new MenuEditor('myList', {listOptions: options, iconPicker: iconPickerOpt, labelEdit: 'Düzəliş'});
                editor.setData(strjson);
                $('#btnReload').on('click', function () {
                    editor.setData(strjson);
                });
 $(document).on('click', '.btnRemove', function (e) {//remove button
        e.preventDefault();
        if (confirm("Silmək istədiyinizə əminsinizmi?")){


                var $category_id=$(this).closest( "li").attr('id');

            

                $.ajax({
                        method: 'post',
                        headers: {
                                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                              },
                        url: "{{url('delete_category')}}",
                        data: {
                            'category_id': $category_id,
                        },
                        async: true,
                        success: function(response){
                           /*  var insertResultJson = response;

                            editor.setData(insertResultJson);*/
                              console.log(response);
                          
                           
                        },
                        error: function(data){
                            console.log(data);
                            alert("fail" + ' ')
                        },

                    });




            
            var list = $(this).closest('ul');
            $(this).closest('li').remove();
            var isMainContainer = false;
            
            if ((!list.children().length) && (!isMainContainer)) {
                list.prev('div').children('.sortableListsOpener').first().remove();
                list.remove();
            }
        }
    });

                 /*$('.btnRemove').on('click', function () {
                    alert($(this).closest( "li").attr('id'));
                });*/
 
                $('#btnOut').on('click', function () {//sort order button
        

                
                    var str = editor.getString();

                   // console.log();
                    $("#out").text(str);

                      $.ajax({
                        method: 'post',
                        headers: {
                                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                              },
                        url: "{{url('save_order')}}",
                        data: {
                            'sort_json_data': str,
                        },
                        async: true,
                        success: function(response){

                              editor.setData(response);
                           /*  var insertResultJson = response;

                            editor.setData(insertResultJson);*/
               
                            $('#successMessage').fadeIn(800);
                            $('#successMessage').delay(1300);
                            $('#successMessage').fadeOut(800);


                              console.log(response);
                          
                           
                        },
                        error: function(data){
                            console.log(data);
                            alert("fail" + ' ')
                        },

                    });

                });


                $('#btnAdd').on('click',function() //add button click
                {
                       //console.log(editor.getString());
             

                      $.ajax({
                        method: 'post',
                        headers: {
                                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                              },
                        url: "{{url('add_category')}}",
                        data: {
                            'json_data': editor.getString(),
                        },
                        async: true,
                        success: function(response){
                            
                            var insertResultJson = response;

                            editor.setData(insertResultJson);

                            console.log(response);
                         /*   alert(response);*/
                           
                        },
                        error: function(data){
                            console.log(data);
                            alert("fail" + ' ')
                        },

                    });
              
                });

$('#btnUpdate').on('click', function () {


    $.ajax({
                        method: 'post',
                        headers: {
                                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                              },
                        url: "{{url('update_category')}}",
                        data: {
                            'update_data': $("#frmEdit").serializeArray(),
                        },
                        async: true,
                        success: function(response){
                        

                            

                             
                              $('#successMessage').fadeIn(300);
                            $('#successMessage').delay(1300);
                            $('#successMessage').fadeOut(800);
                         /*   alert(response);*/
                           
                        },
                        error: function(data){
                            console.log(data);
                            alert("fail" + ' ')
                        },

                    });


    reset();
})

                function reset()
                {
                    $("#frmEdit")[0].reset();
                                            iconPicker = $('#mnu_iconpicker').iconpicker(iconPickerOpt);
                                            iconPicker.iconpicker('setIcon', 'empty');
                                            $("#btnUpdate").attr('disabled', true);
                                            itemEdit = 0;
                }
            });
        </script>
        @endsection