@section('head')
<link rel="Stylesheet" type="text/css" href="{{asset('theme/admin/crop/sweetalert.css')}}" />
        <link rel="Stylesheet" type="text/css" href="{{asset('theme/admin/crop/croppie.css')}}" />
        <link rel="Stylesheet" type="text/css" href="{{asset('theme/admin/crop/demo/demo.css')}}" />
 
@endsection
 

<?php
$desc_az=(isset($edit_boolean) && $edit_boolean==true)? old( 'desc_az', $teacher->desc_az) :old('desc_az');
$desc_en=(isset($edit_boolean) && $edit_boolean==true)?old( 'desc_en', $teacher->desc_en):old('desc_en');
$desc_ru=(isset($edit_boolean) && $edit_boolean==true)?old( 'desc_ru', $teacher->desc_ru):old('desc_ru');
 
$course_az=(isset($edit_boolean) && $edit_boolean==true)?old( 'course_az', $teacher->course_az):old('course_az');
$course_en=(isset($edit_boolean) && $edit_boolean==true)?old( 'course_en', $teacher->course_en):old('course_en');
$course_ru=(isset($edit_boolean) && $edit_boolean==true)?old( 'course_ru', $teacher->course_ru):old('course_ru');

$fullname_az=(isset($edit_boolean) && $edit_boolean==true)?old( 'fullname_az', $teacher->fullname_az):old('fullname_az');
$fullname_en=(isset($edit_boolean) && $edit_boolean==true)?old( 'fullname_en', $teacher->fullname_en):old('fullname_en');
$fullname_ru=(isset($edit_boolean) && $edit_boolean==true)?old( 'fullname_ru', $teacher->fullname_ru):old('fullname_ru');

$image=(isset($edit_boolean) && $edit_boolean==true)?old( 'image_cropped_base64',(string) Image::make(asset("/storage/team/".$teacher->image))->encode('data-url')):old('image_cropped_base64');


 
 

 

 ?>






@foreach ($errors->all() as $message) 

<div class="alert alert-danger" role="alert">

  <span class="sr-only">Error:</span>
 {{$message}}

</div>

@endforeach
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#az"><img src="{{asset('theme/admin/images/az.png')}}" width="50">&nbsp;&nbsp;Azərbaycanca</a></li>
    <li><a data-toggle="tab" href="#ru"><img src="{{asset('theme/admin/images/ru.png')}}" width="50">&nbsp;&nbsp;Русский</a></li>
      <li><a data-toggle="tab" href="#en"><img src="{{asset('theme/admin/images/en.png')}}" width="50">&nbsp;&nbsp;English</a></li>
  </ul>
 







      <div class="tab-content">
      <div class="demo-wrap upload-demo">
                    <div class="container">
                    <div class="grid">
                       
                        <div class="col-1-1">
                            <div class="upload-msg">
                                Upload a file to start cropping
                            </div>
                            <div class="upload-demo-wrap">
                             <div id="upload-demo">
                             	
                             </div>
                            </div>
							
							 <div class="actions">
                           <center><a class="btn file-btn">
                                    <span>Upload</span>
                                    <input type="file" name='photo' id="upload" value="Choose a file" accept="image/*" />
                                </a>
                                <a class="upload-result btn file-btn">Save</a>

  				 
                                </center>    
                            </div>
                        </div>
						
                    </div>
                </div>
                  <textarea class="image_cropped" style="display: none;" name='image_cropped_base64'> {{$image}}</textarea>
                </div>
     
 
        <div id="az" class="tab-pane fade in active">
         <div class="form-group"><br>
          <label for="comment">Kurs:</label>
          <input type="text" class="form-control" value="{{$course_az}}" id="course_az" name="course_az">
        </div>
            <div class="form-group"><br>
          <label for="comment">Tam adı:</label>
          <input type="text" class="form-control" value="{{$fullname_az}}" id="fullname_az" name="fullname_az">
        </div>
         <div class="form-group"><br>
          <label for="comment">Daha ətraflı:</label>
          <textarea class="form-control" name="desc_az" style="height: 350px" id="desc_az">{{$desc_az}}</textarea>
        </div>
      </div>
       <div id="ru" class="tab-pane fade in">
       <div class="form-group"><br>
          <label for="comment">Курс который преподаёт:</label>
          <input type="text" class="form-control" value="{{$course_ru}}" id="course_ru" name="course_ru">
        </div>
         <div class="form-group"><br>
          <label for="comment">Полное имя:</label>
          <input type="text" class="form-control" value="{{$fullname_ru}}" name="fullname_ru">
        </div>
         <div class="form-group"><br>
          <label for="comment">Подробно:</label>
          <textarea class="form-control" rows="27" name="desc_ru" style="height: 350px" id="desc_ru">{{$desc_ru}}</textarea>
        </div>
      </div>
      <div id="en" class="tab-pane fade in">
         <div class="form-group"><br>
          <label for="comment">Lecturer in course:</label>
          <input type="text" class="form-control" value="{{$course_en}}" name="course_en">
        </div>
            <div class="form-group"><br>
          <label for="comment">Fullname:</label>
          <input type="text" class="form-control" value="{{$fullname_en}}" name="fullname_en">
        </div>
         <div class="form-group"><br>
          <label for="comment">More info:</label>
          <textarea class="form-control" rows="27" name="desc_en" style="height: 350px" id="desc_en">{{$desc_en}}</textarea>
        </div>
      </div>
      

    </div>
<div class="row">
    <div class="col-xs-12">
        <div class="text-right">
            <input type="submit"  class="btn btn-primary" value="Yadda saxla">
        </div>
    </div>
</div>






















 @section('bottom')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 
        <script src="{{asset('theme/admin/crop/demo/prism.js')}}"></script>
        <script src="{{asset('theme/admin/crop/sweetalert.min.js')}}"></script>

        <script src="{{asset('theme/admin/crop/croppie.js')}}"></script>
        <script src="{{asset('theme/admin/crop/demo/demo.js')}}"></script>
        <script src="{{asset('theme/admin/crop/bower_components/exif-js/exif.js')}}"></script>
        <script>
 /*           $( document ).ready(function() {
  $(".reset").click(function () {
    $('.upload-demo').removeClass('ready');
    $('#upload').val(''); // this will clear the input val.

    $('#upload-demo').html('<div class="cr-boundary" aria-dropeffect="none" style=""></div><div class="cr-overlay"></div></div><div class="cr-slider-wrap"><input class="cr-slider" step="0.0001" aria-label="zoom" type="range"></div>');


    $uploadCrop.croppie('bind', {
        url : ''
    }).then(function () {
        console.log('reset complete');
    });
});
});*/
 
            Demo.init();

      


        </script>

 


@endsection