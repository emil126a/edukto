@extends('layouts.app')
    @section('guest_page_url', url('/homepage'))
@section('head')
 <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js'></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('bs-iconpicker/css/bootstrap-iconpicker.min.css')}}">
        


        <link rel="stylesheet" type="text/css" href="{{asset('theme/admin/dropzone/dropzone.css')}}">


        <script src="{{asset('theme/admin/tinymce/tinymce.min.js')}}"></script>
        
 <style type="text/css">
  
  form
  {
    width:93%;
    margin: 0 auto;
  }

   

</style>

<script>
 

tinymce.init({
 
  selector: "textarea",
  language: '{{App::getLocale()}}',
  content_css : "{{asset('theme/guest/css/style.css')}},{{asset('theme/guest/css/color.css')}}", 
  plugins: [
    "advlist autolink lists link image charmap print preview anchor textcolor colorpicker",
    "searchreplace visualblocks fullscreen",
    "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image forecolor backcolor",
    
    // Allow image drag and drop
    // Dropped images are embedded with data uris
    //http://www.tinymce.com/wiki.php/Configuration:paste_data_images
  paste_data_images: true,
    image_advtab: true,

});


</script>


@endsection
 
@section('content')

<!-- <img src="{{asset('storage/content_images/OLi31e2PWt0DTAbV4jTsqmsAEueZwbohxB1F3Q51.jpeg')}}"> -->
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading"><h4> </h4></div>
                <div class="panel-body">

 

{!! Form::open(['url' => 'admin/upload_content_images', 'class'=>"dropzone sortable",   'id'=>"my-awesome-dropzone"]) !!} 



 
        
<div class="dz-message" data-dz-message><span><b>Mövzu ilə əlaqəli şəkilləri çəkib bura atın (əgər varsa)</b></span></div>



{!! Form::close() !!}

<br><br><br>










{!! Form::open(['url' => 'admin/add_content']) !!} 
@if(Session::has('message')) 

  <div class="alert alert-success" role="alert">
     
    <strong>Diqqət!</strong> {{{Session::get('message')}}}
  </div>

@endif
<div class="sorting_result">
  


</div>
<div id="add_image">
  

@if(!is_null($kept_content_images))

 
                  @foreach($kept_content_images as $key=>$content_image)
                  <input name="content_images[]" value="{{$content_image}}" type="text">

                  @endforeach

   @endif

</div>
   

 
@foreach ($errors->all() as $message) 

<div class="alert alert-danger" role="alert">

  <span class="sr-only">Error:</span>
 {{$message}}

</div>

@endforeach
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#az"><img src="{{asset('theme/admin/images/az.png')}}" width="50">&nbsp;&nbsp;Azərbaycanca</a></li>
    <li><a data-toggle="tab" href="#ru"><img src="{{asset('theme/admin/images/ru.png')}}" width="50">&nbsp;&nbsp;Русский</a></li>
      <li><a data-toggle="tab" href="#en"><img src="{{asset('theme/admin/images/en.png')}}" width="50">&nbsp;&nbsp;English</a></li>
  </ul>
    <div class="form-group"><br>
          <label for="comment">Kateroriya:</label>
             {{ Form::select('category_id', [''=>'---------']+$categories ,null,['class'=>'form-control','id'=>'category_id']) }}
        </div>
      <div class="tab-content">
        <div id="az" class="tab-pane fade in active">
         <div class="form-group"><br>
          <label for="comment">Başlıq:</label>
          <input type="text" class="form-control" value="{{old('title_az')}}" name="title_az">
        </div>
         <div class="form-group"><br>
          <label for="comment">Mətn:</label>
          <textarea class="form-control" name="desc_az" style="height: 350px" id="desc_az">
             {{old('desc_az')}}
          </textarea>
        </div>
      </div>
       <div id="ru" class="tab-pane fade in">
         <div class="form-group"><br>
          <label for="comment">Заглавие:</label>
          <input type="text" class="form-control" value=" {{old('title_ru')}}" name="title_ru">
        </div>
         <div class="form-group"><br>
          <label for="comment">Текст :</label>
          <textarea class="form-control" rows="27" name="desc_ru" style="height: 350px" id="desc_ru">
           {{old('desc_ru')}}
          </textarea>
        </div>
      </div>
      <div id="en" class="tab-pane fade in">
         <div class="form-group"><br>
          <label for="comment">Title:</label>
          <input type="text" class="form-control" value="{{old('title_en')}}" name="title_en">
        </div>
         <div class="form-group"><br>
          <label for="comment">Text:</label>
          <textarea class="form-control" rows="27" name="desc_en" style="height: 350px" id="desc_en">
             {{old('desc_en')}}
          </textarea>
        </div>
      </div>
      

    </div>
<div class="row">
    <div class="col-xs-12">
        <div class="text-right">
            <input type="submit"  class="btn btn-primary" value="Yadda saxla">
        </div>
    </div>
</div>
</form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>








  


$(document).ready(function(){
  
 

@foreach (\App\Content::all() as $cat)

 $("#category_id option[value='{{$cat->category_id}}']").css('color','blue').prop('disabled','disabled');


@endforeach



@foreach (\App\Content::all() as $cat)

 $("#category_id option[value='{{$cat->category_id}}']").css('color','blue').prop('disabled','disabled');


@endforeach


 @php

$used_cats=\App\Category::where('used','1')->get();

@endphp

 @foreach ($used_cats as $used_cat)

 $("#category_id option[value='{{$used_cat->id}}']").css('color','blue').prop('disabled','disabled');


@endforeach
 // $("select option[value='74']").prop('disabled','disabled');
 
});
</script>


@endsection
@include('dashboard.dropzone_part_content', ['folder' => 'content'])
 
