@extends('layouts.guest')

 @section('head')

{{Html::style('theme/guest/css/lightslider.min.css')}}
<style type="text/css">
 
 
img {
    display: block;
    height: auto;
    max-width: 100% !important;
    margin: 0 auto;

}
</style>
 @endsection

 
@section('content')
    <?php
                                   $desc='desc_'.app()->getLocale();
                                   $title='title_'.app()->getLocale();
                                 ?>
 

        <!-- About Slider Area Start Here -->
        <div class="about-slider-area">
         
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

 <h2 class="title-default-left title-bar-high">{{$page->$title}}</h2>
                        <div class="row">



                        @if(count($page->images)>0)
                            <div class="col-md-4 col-sm-6">
                                <div>
                            


    <ul id="lightSlider">

    @foreach($page->images as $image)

    @php

    $big_image=asset('storage/content_images/big_images/'.$image->image);
    $small_image=asset('storage/content_images/thumbnail/'.$image->image);
    @endphp
        <li data-thumb=" {{$small_image}}" class="lslide active">
           <a data-fancybox="images" href=" {{$big_image}}">
 <div>
            <span style="   top: 0px; z-index: 555; position: absolute;    display:inline-block; background-color: rgba(0, 0, 0, 0.5); color: rgb(255, 255, 255); font-size: 12px;  ">{{trans('common.click_to_maximize')}}</span>
             
             

             <img src="{{$big_image}}" style="display: block; object-fit: contain; margin: 0 auto; height: 355px !important;" > 
 </div>
            </a>
        </li>
        
     
        
            @endforeach    
        
    </ul>
</div>
                            </div>

                            @endif
                            <!-- .product -->
                            @php

                            $column_md=(count($page->images))?'8':'12';
                            $column_sm=(count($page->images))?'6':'12';
                            @endphp
                            <div class="col-md-{{$column_md}} col-sm-{{$column_sm}}">
                                 
                                

                               {!!$page->$desc!!}
                           
                                    
                                
                               
                              
                                 
                            </div>
                        </div>
                        
                    </div>
                </div>
           
            </div>
        </div>
    @endsection



    @section('bottom')

   {{Html::script('theme/guest/js/lightslider.js')}}

<script type="text/javascript">
    
    $('#lightSlider').lightSlider({
    gallery: true,
    item: 1,
    loop: false,
    slideMargin: 0,
    thumbItem: 9
});
</script>
<link rel="stylesheet" href="{{asset('theme/guest/css/jquery.fancybox.css')}}" />
<script src="{{asset('theme/guest/js/jquery.fancybox.js')}}"></script>
    @endsection