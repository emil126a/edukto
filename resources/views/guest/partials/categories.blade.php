@php

$dropdown_class=(count( $category['children'])>0)?'class="dropdown"':'';
$caret= (count( $category['children'])>0)?'<b class="caret"></b>':''; 
$href=(count( $category['children'])>0)?'class="dropdown-toggle"':''; 

@endphp

 
<li {!!$dropdown_class!!}>

<a href="{{url(app()->getLocale().'/'.$category['slug'])}}" {!! $href !!} target="{{$category['target']}}" >  
  


@if($category['icon']!='empty')
<i class="{{ $category['icon'] }}" aria-hidden="true" style="left:8px;"></i>&nbsp;

@endif


   {{ $category['text_'.app()->getLocale()] }} 


    {!!$caret!!}


 </a>



@if(count( $category['children'])>0)
<ul class="dropdown-menu">
 	@foreach($category['children'] as $category)
 			@include('guest.partials.categories',$category)

 	@endforeach
 </ul>
 @endif						 
													</li>

	 