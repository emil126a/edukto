@extends('layouts.guest')

 

 
@section('content')


<style>
#map {
        width: 100%;
        height: 250px;
     }
    </style>

   
    <script>
      function initMap() {
 
        var mapDiv = document.getElementById('map');
        var myLatLng = {lat:40.40764,  lng: 49.88796};

        var map = new google.maps.Map(mapDiv, {
            center:myLatLng,
            zoom: 18
        });

         var marker = new google.maps.Marker({
          position: myLatLng,
          icon: '{{asset("theme/guest/img/map-icon.png")}}',
          map: map,
          title: 'Edukto təhsil mərkəzi!'
        });
      }
    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATn-lotYpOKHdxyIHjX4i7rTvJShmgjd4&callback=initMap">
    </script>

<div class="container-fluid">
                <div class="row">
                    <div class="google-map-area">
                          <div id="map"></div>
                    </div><br><br>
                </div>
            </div>
 
 <!-- Contact Us Page 2 Area Start Here -->
        <div class="contact-us-page2-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h2 class="title-default-left title-bar-high">{{trans('common.address')}}</h2>
                        <div class="contact-us-info2">
                            <ul>
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i>{{trans('common.street')}}</li>
                                <li><i class="fa fa-phone" aria-hidden="true"></i>+99455 840 41 40</li>
                                <li><i class="fa fa-envelope-o" aria-hidden="true"></i>info@edukto.com</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2 class="title-default-left title-bar-high">{{trans('common.contact_us')}}</h2>
                            </div>
                        </div>
                        <div class="row">
                            

                            <div class="contact-form2">
                             @if(Session::has('message'))
              <div class="alert alert-success" role="alert">
     
    {!!Session::get('message')!!}
  </div>

  @endif
{{Form::open(['id'=>'contact-form'])}}
                              
                                    <fieldset>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" placeholder="{{trans('common.name')}}" value="{{old('name')}}" class="form-control" name="name" id="form-name" >
                                                <div class="help-block with-errors" style="color:red">  {{ $errors->first('name') }} </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" placeholder="{{trans('common.email')}}" class="form-control" value="{{old('email')}}" name="email" id="form-email" >
                                                <div class="help-block with-errors" style="color:red"> {{ $errors->first('email') }}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <textarea placeholder="{{trans('common.message')}}" class="textarea form-control" name="message" id="form-message" rows="8" cols="20">{{old('message')}}</textarea>
                                                <div class="help-block with-errors" style="color:red">{{ $errors->first('message') }}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-sm-12">
                                            <div class="form-group margin-bottom-none">
                                                <button type="submit" class="default-big-btn">{{trans('common.send')}}</button>
                                            </div>
                                        </div>
                                        
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

        </div>
        
               <br><br><br><br>                    
        <!-- Contact Us Page 2 Area End Here -->
    @endsection


