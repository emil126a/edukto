@extends('layouts.guest')

 

@section('content')


<?php

$fullname='fullname_'.App::getLocale();
$course='course_'.App::getLocale();
$desc='desc_'.App::getLocale();

?>
 <!-- Inner Page Banner Area Start Here -->
        <div class="inner-page-banner-area" style="background-image: url('{{asset("theme/guest/img/banner/teachers.jpg")}}');">
            <div class="container">
                <div class="pagination-area">
                    <h1>{{$teacher->$fullname}}</h1>
                    <ul>
                        <li><a href="/">{{trans('common.home')}}</a> -</li>
                        <li>{{trans('common.teachers')}}</li>
                    </ul>
                </div>
            </div>
        </div>
         <div class="lecturers-page-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="lecturers-contact-info">
                            <img src="{{asset('storage/team/'.$teacher->image)}}" class="img-responsive" alt="team">
                            <h2>{{$teacher->$fullname}}</h2>
                            <h3>{{$teacher->$course}}</h3>
                             
                             
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                        <h3 class="title-default-left title-bar-big-left-close">{{trans('common.about_teacher')}} </h3>
                        <p>{{$teacher->$desc}}</p>
                       
                        
                     
                    </div>
                </div>
            </div>
        </div>
    @endsection

 