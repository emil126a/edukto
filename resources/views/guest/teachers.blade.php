@extends('layouts.guest')

 

@section('content')
<?php

$fullname='fullname_'.App::getLocale();
$course='course_'.App::getLocale();
$desc='desc_'.App::getLocale();

?>

 <!-- Inner Page Banner Area Start Here -->
        <div class="inner-page-banner-area" style="background-image: url('{{asset("theme/guest/img/banner/teachers.jpg")}}');">
            <div class="container">
                <div class="pagination-area">
                    <h1>{{trans('common.our_prof_lectures')}}</h1>
                    <ul>
                       <li><a href="/">{{trans('common.home')}}</a> -</li>
                        <li>{{trans('common.teachers')}}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Inner Page Banner Area End Here -->
    <div class="lecturers-page1-area">
            <div class="container">
                <div class="row">
                @foreach($teachers as $teacher)

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="single-item">
                            <div class="lecturers1-item-wrapper">
                                <div class="lecturers-img-wrapper">
                                    <a href="{{url(App::getLocale().'/teacher/'.$teacher->id)}}"><img class="img-responsive" src="{{asset('storage/team/'.$teacher->image)}}" alt="team"></a>
                                </div>
                                <div class="lecturers-content-wrapper">
                                    <h3 class="item-title"><a href="{{url('teacher/'.$teacher->id)}}">{{$teacher->$fullname}}</a></h3>
                                    <span class="item-designation">{{$teacher->$course}}</span>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                     
                  
                </div>
                {{ $teachers->links('guest.pagination') }}
                 
            </div>
        </div>
        <!-- Lecturers Page 1 Area End Here -->
    @endsection

 