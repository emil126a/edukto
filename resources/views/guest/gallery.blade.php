@extends('layouts.guest')

 

 
@section('content')

 <!-- Inner Page Banner Area Start Here -->
        <div class="inner-page-banner-area" style="background-image: url('{{asset("theme/guest/img/banner/5.jpg")}}');">
            <div class="container">
                <div class="pagination-area">
                    <h1>{{trans('common.gallery')}}</h1>
                    <ul>
                        <li><a href="/">{{trans('common.home')}}</a> -</li>
                        <li>{{trans('common.gallery')}}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Inner Page Banner Area End Here -->
        <!-- Gallery Area 1 Start Here -->
        <div class="gallery-area1">
            <div class="container">
                <div class="row gallery-wrapper">
                @foreach($gallery as $image)

<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" >
                        <div class="gallery-box">
                      
                            <img src="{{asset('storage/gallery/'.$image->name)}}" class="img-responsive" alt="gallery" style="display: block; object-fit: contain; margin: 0 auto; height: 377px !important;">
                            <div class="gallery-content">
                                <a href="{{asset('storage/gallery/'.$image->name)}}" class="zoom"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
                     
                     
                     
                </div>
            </div>



{{ $gallery->links('guest.pagination') }}
                                             
        </div>
        <!-- Gallery Area 1 End Here -->

    @endsection

@section('bottom')

    <script src="{{asset('theme/guest/js/jquery.magnific-popup.min.js')}}" type="text/javascript"></script>
@endsection