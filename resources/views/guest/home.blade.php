@extends('layouts.guest')

 
 
 
@section('content')
    <div class="slider1-area overlay-default index1">
            <div class="bend niceties preview-1">
                <div id="ensign-nivoslider-3" class="slides">
                    <img src="{{asset('theme/guest/img/slider/1-1.jpg')}}" alt="slider" title="#slider-direction-1" />
                    <img src="{{asset('theme/guest/img/slider/1-2.jpg')}}" alt="slider" title="#slider-direction-2" />
                    <img src="{{asset('theme/guest/img/slider/1-3.jpg')}}" alt="slider" title="#slider-direction-3" />
                </div>
                <div id="slider-direction-1" class="t-cn slider-direction">
                    <div class="slider-content s-tb slide-1">
                        <div class="title-container s-tb-c">
                            <div class="title1">{{trans('common.lang_bridge')}}</div>
                       
                             
                        </div>
                    </div>
                </div>
              
                <div id="slider-direction-3" class="t-cn slider-direction">
                    <div class="slider-content s-tb slide-3">
                        <div class="title-container s-tb-c">
                            <div class="title1">{{trans('common.have_question')}}</div>
                            <p>{!! trans('common.offer_solution') !!}</p>
                            <div class="slider-btn-area">
                                <a href="#" class="default-big-btn">{{trans('common.contact_us')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Slider 1 Area End Here -->
         
       
  
    <div class="service1-area">
            <div class="service1-inner-area">
                <div class="container">
                    <div class="row service1-wrapper">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 service-box1" style="min-height: 200px;">
                            <div class="service-box-content">
                                <h3><a href="#">{{trans('common.english_lessons')}}</a></h3>
                                <p>
                                   {{trans('common.english_lessons_desc')}} 
                               </p>
                            </div>
                            <div class="service-box-icon">
                                <i class="fa fa-book" aria-hidden="true"></i>

                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 service-box1" style="min-height: 200px;">
                            <div class="service-box-content">
                                <h3><a href="#">{{trans('common.education_abroad')}}</a></h3>
                                <p>{{trans('common.education_abroad_desc')}}</p>
                            </div>
                            <div class="service-box-icon">
                                <i class="fa fa-university" aria-hidden="true"></i>

                            </div>
                        </div>
                        <!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 service-box1">
                            <div class="service-box-content">
                                <h3><a href="#">  Yay Düşərgəsi</a></h3>
                                <p>Peşəkar və təcrübəli heyətimizin təşkilatçılığı ilə uşaq, yeniyetmə və gənclər üçün nəzərdə tutulmuş yay düşərgəsinə qəbul başlandı!</p>
                            </div>
                            <div class="service-box-icon">
                                
                                
 <i class="fa fa-users" aria-hidden="true"></i>
                                
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
         <!-- Certificate Area Start Here -->
 <?php

$fullname='fullname_'.App::getLocale();
$course='course_'.App::getLocale();
 

?>
        
      <div class="lecturers-area">
            <div class="container">
                <h2 class="title-default-left">{{trans('common.our_prof_lectures')}}</h2>
            </div>
            <div class="container">
                <div class="rc-carousel" data-loop="true" data-items="4" data-margin="30" data-autoplay="false" data-autoplay-timeout="10000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-r-x-small="1" data-r-x-small-nav="true" data-r-x-small-dots="false" data-r-x-medium="2" data-r-x-medium-nav="true" data-r-x-medium-dots="false" data-r-small="3" data-r-small-nav="true" data-r-small-dots="false" data-r-medium="4" data-r-medium-nav="true" data-r-medium-dots="false" data-r-large="4" data-r-large-nav="true" data-r-large-dots="false">
                    

                @foreach($teachers as $teacher)

 <div class="single-item">
                        <div class="lecturers1-item-wrapper">
                            <div class="lecturers-img-wrapper">
                                <a href="{{url(App::getLocale().'/teacher/'.$teacher->id)}}"><img class="img-responsive" style="display: block; object-fit: contain; margin: 0 auto; height: 210px !important;" src="{{asset('storage/team/'.$teacher->image)}}" alt="team"></a>
                            </div>
                            <div class="lecturers-content-wrapper">
                                <h3 class="item-title"><a href="{{url(App::getLocale().'/teacher/'.$teacher->id)}}">{{$teacher->$fullname}}</a></h3>
                                <span class="item-designation">{{$teacher->$course}}</span>
                               
                            </div>
                        </div>
                    </div>

                @endforeach
                   
                    
                     
                   
                     
                </div>
            </div>
        </div>
        <!-- Lecturers Area End Here -->

    @endsection