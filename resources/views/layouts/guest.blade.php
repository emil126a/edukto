<!doctype html>
<html class="no-js" lang="">
<head>
<style>
.navbar-static-top{
border-width:0px;
}

.navbar
{
    margin-bottom: 0px !important;
}
.navbar-nav
{
    float:right !important;
}
 .navbar-default
 {
    background:none !important;
 }
 
 .navbar-default .navbar-nav > li > a
 {
 padding: 30px 17px;
 display: block;
text-transform: uppercase;
text-decoration: none  !important;;
color: #002147 !important;
font-size: 1vw;
font-weight: 500 !important;
   transition: all .5s ease-out !important;
 }
 
 .navbar-default .navbar-nav > li > a:hover {
    background-color: #f7c807 !important;
    color: white;
}


.dropdown-menu > li.kopie > a {
    padding-left:5px;
}
 
.dropdown-submenu {
    position:relative;
}
.dropdown-submenu>.dropdown-menu {
   top:0;left:100%;
   margin-top:-6px;margin-left:-1px;
   -webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;
   
   
 }
  
.dropdown-submenu > a:after {
  border-color: transparent transparent transparent white;
  border-style: solid;
  border-width: 5px 0 5px 5px;
  content: " ";
  display: block;
  float: right;  
  height: 0;     
  margin-right: -10px;
  margin-top: 5px;
  width: 0;
}
 
.dropdown-submenu:hover>a:after {
    border-left-color:#555;
 }

.dropdown-menu > li
{
background-color:#2357a3 !important;
padding: 32;


}


 .dropdown-menu > li > a
 {
 padding: 10px 25px !important;
 color:white !important;
   transition: all .5s ease-out !important;
 }
 
 
.dropdown-menu > li > a:hover, .dropdown-menu > .active > a:hover {
  text-decoration: underline;
  color:#fdc800 !important;
   
}  
  
@media (max-width: 767px) {

  .navbar-nav  {
     display: inline;
  }
  .navbar-default .navbar-brand {
    display: inline;
  }
  .navbar-default .navbar-toggle .icon-bar {
    background-color: #fff;
  }
  .navbar-default .navbar-nav .dropdown-menu > li > a {
    color: red;
    background-color: #ccc;
    border-radius: 4px;
    margin-top: 2px;   
  }
   .navbar-default .navbar-nav .open .dropdown-menu > li > a {
     color: #002147;
   }
   .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
   .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
     background-color: #ccc;
   }

   .navbar-nav .open .dropdown-menu {
     border-bottom: 1px solid white; 
     border-radius: 0;
   }
  .dropdown-menu {
      padding-left: 10px;
  }
  .dropdown-menu .dropdown-menu {
      padding-left: 20px;
   }
   .dropdown-menu .dropdown-menu .dropdown-menu {
      padding-left: 30px;
   }
   li.dropdown.open {
    border: 0px solid red;
   }

}
 
@media (min-width: 768px) {
  ul.nav li:hover > ul.dropdown-menu {
    display: block;
  }
  #navbar {
    text-align: center;
  }
  
  .dropdown-menu
  {

  padding: 1px 0 !important;
  }
}  

 /* Sticky footer styles
-------------------------------------------------- */
html {
  position: relative;
  min-height: 100%;
}
body {
  /* Margin bottom by footer height */
  margin-bottom: 60px;
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  /* Set the fixed height of the footer here */
  height: 60px;
  line-height: 60px; /* Vertically center the text there */
  background-color: #f5f5f5;
}


/* Custom page CSS
-------------------------------------------------- */
/* Not required for template or sticky footer method. */

body > .container {
  padding: 60px 15px 0;
}

.footer > .container {
  padding-right: 15px;
  padding-left: 15px;
}

code {
  font-size: 80%;
}
 
 
 /*-- Footer Social --*/
.footer-social li {
 display: inline;
}
.footer-social a {
  color: #ddd;
  display: inline-block;
  margin-right: 15px;
}
.footer-social a:hover {
  color: #ffae00;
}
.footer-social a i {
  display: block;
  font-size: 27px;
  line-height: 17px;
}

@media only screen and (max-width:691px) {
    .slider1-area {
       color:red;
      
    }
}
</style>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Edukto - Education Abroad and Training Center</title>
    <meta name="description" content="Education Center & Training">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="{{asset('theme/guest/css/normalize.css')}}">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{asset('theme/guest/css/main.css')}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('theme/guest/css/bootstrap.min.css')}}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{asset('theme/guest/css/animate.min.css')}}">
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="{{asset('theme/guest/css/font-awesome.min.css')}}">
    <!-- Owl Caousel CSS -->
    <link rel="stylesheet" href="{{asset('theme/guest/vendor/OwlCarousel/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme/guest/vendor/OwlCarousel/owl.theme.default.min.css')}}">
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="{{asset('theme/guest/css/meanmenu.min.css')}}">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="{{asset('theme/guest/vendor/slider/css/nivo-slider.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('theme/guest/vendor/slider/css/preview.css')}}" type="text/css" media="screen" />
    <!-- Datetime Picker Style CSS -->
    <link rel="stylesheet" href="{{asset('theme/guest/css/jquery.datetimepicker.css')}}">
    <!-- Magic popup CSS -->
    <link rel="stylesheet" href="{{asset('theme/guest/css/magnific-popup.css')}}">
    <!-- Switch Style CSS -->
    <link rel="stylesheet" href="{{asset('theme/guest/css/hover-min.css')}}">
    <!-- ReImageGrid CSS -->
    <link rel="stylesheet" href="{{asset('theme/guest/css/reImageGrid.css')}}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('theme/guest/css/style.css')}}">
    <!-- Modernizr Js -->
    <script src="{{asset('theme/guest/js/modernizr-2.8.3.min.js')}}"></script>
    @yield('head')
</head>

<body>

    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <!-- Main Body Area Start Here -->
    <header>
            <div id="header2" class="header2-area">
                <div class="header-top-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="header-top-left">
                                    <ul>
                                        <li><i class="fa fa-phone" aria-hidden="true"></i><a href="Tel:+994708255079"> (012) 404-75-70 </a></li>
                                        <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#">info@edukto.com</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="header-top-right">

                                    <ul style="text-transform: uppercase;">
                                     @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
        <li>
            <a rel="alternate" class="login-btn-area"  hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                {{ $localeCode }}
            </a>
        </li>
    @endforeach
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-menu-area bg-textPrimary" id="sticker">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-3">
                                <div class="logo-area">
                                    <a href="{{url('/')}}"><img class="img-responsive" src="{{asset('theme/guest/img/logo-primary.png')}}" alt="logo"></a>
                                </div>
                            </div>
                            
                            <div class="col-lg-9 col-md-9 col-sm-9">
                              
<div id="navbar">    
  <nav class="navbar navbar-default navbar-static-top" role="navigation">
             
            
            <div class="collapse navbar-collapse" id="navbar-collapse-1">
                <ul class="nav navbar-nav">
                 @foreach($categories as $category)


@include('guest.partials.categories',$category)

@endforeach

              
                   
                  
                  
                   
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
</div>


            
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu Area Start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                         
                                      @foreach($categories as $category)


@include('guest.partials.mobile_categories',$category)

@endforeach

                                       
                                     
                                        
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Mobile Menu Area End -->
        </header>


     
        
       @yield('content')
        
        <!-- Courses 1 Area End Here -->
        
    <!-- Main Body Area End Here -->
    <div class="clearfix"></div>
    <div class="clearfix"></div>

    <div class="clearfix"></div>
    
    <!-- Footer Area Start Here -->
        <footer class='footer'>
             
            <div class="footer-area-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <p>&copy; Edukto Education Center</p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <ul class="footer-social" style='float:right'>
                                <li>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                
                                <li>
                                   <a href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Area End Here -->
    <!-- jquery-->
    <script src="{{asset('theme/guest/js/jquery-2.2.4.min.js')}}" type="text/javascript"></script>

    <script type="text/javascript">
       $(document).ready(function()
       {
            $('nav#dropdown').meanmenu({ siteLogo: "<a href='index.html' class='logo-mobile-menu'><img src='"+"{{asset('theme/guest/img/logo-primary.png')}}"+"' /></a>" });
       })
 
    </script>


    <!-- Plugins js -->
    <script src="{{asset('theme/guest/js/plugins.js')}}" type="text/javascript"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('theme/guest/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <!-- WOW JS -->
    <script src="{{asset('theme/guest/js/wow.min.js')}}"></script>
    <!-- Nivo slider js -->
    <script src="{{asset('theme/guest/vendor/slider/js/jquery.nivo.slider.js')}}" type="text/javascript"></script>
    <script src="{{asset('theme/guest/vendor/slider/home.js')}}" type="text/javascript"></script>
    <!-- Owl Cauosel JS -->
    <script src="{{asset('theme/guest/vendor/OwlCarousel/owl.carousel.min.js')}}" type="text/javascript"></script>
    <!-- Meanmenu Js -->
    <script src="{{asset('theme/guest/js/jquery.meanmenu.min.js')}}" type="text/javascript"></script>
    <!-- Srollup js -->
    <script src="{{asset('theme/guest/js/jquery.scrollUp.min.js')}}" type="text/javascript"></script>
    <!-- jquery.counterup js -->
<!--     <script src="{{asset('theme/guest/js/jquery.counterup.min.js')}}"></script> -->
<!--     <script src="{{asset('theme/guest/js/waypoints.min.js')}}"></script> -->
    <!-- Countdown js -->
    <script src="{{asset('theme/guest/js/jquery.countdown.min.js')}}" type="text/javascript"></script>
    <!-- Isotope js -->
    <script src="{{asset('theme/guest/js/isotope.pkgd.min.js')}}" type="text/javascript"></script>
    <!-- Magic Popup js -->
<!--     <script src="{{asset('theme/guest/js/jquery.magnific-popup.min.js')}}" type="text/javascript"></script> -->
    <!-- Gridrotator js -->
    <script src="{{asset('theme/guest/js/jquery.gridrotator.js')}}" type="text/javascript"></script>
    <!-- Custom Js -->
    <script src="{{asset('theme/guest/js/main.js')}}" type="text/javascript"></script>
        @yield('bottom')
</body>

</html>
