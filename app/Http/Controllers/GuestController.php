<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB, Mail;
class GuestController extends Controller
{
   public function show($slug)
   {
   		$page=\App\Content::with(['images'=>function($query){

$query->orderBy('sort_order_number','ASC');

      }])->with('category')->whereHas('category', function ($query) use ($slug) {
    $query->where('slug', $slug);
})->first();
   		//  dd($page->toArray());
   		if(count($page)>0)//if article exist in DB
   		{


 
   			
   			return	$this->content(compact('page'));
   			
   		}
   		else // if it does not exist it will check related function existense
   		{

   			$category=\App\Category::doesntHave('content')->where('slug',$slug)->first();
   			
//   			 dd($category);
	 
   			if(count($category)>0 && method_exists($this, $category->slug))// if it has category but no content in db
   			{
   				$method=$category->slug;
	            return $this->$method();
   			}
	      	else
	      	{
	      		abort(404);
	      	}
	      	
	     
   		}
   }


   private function content($data)
   {
   		return view('guest.content',$data);
   }


   
    private function contact()
    {
         return view('guest.contact');
    }



   public function homepage()
   {
    
    $data['teachers']=\App\Teacher::all();
     return view('guest.home',$data);
   }

   private function gallery()
   {

    $data['gallery']=\App\Gallery::orderBy(DB::raw('ISNULL(order_number), order_number'), 'ASC')->paginate(9);
    return view('guest.gallery',$data);
   }


   private function teachers()
   {
    $data['teachers']=\App\Teacher::orderBy('id','DESC')->paginate(12);
    return view('guest.teachers',$data);
   }


   public function teacher($id)
   {
    $data['teacher']=\App\Teacher::find($id);
        return view('guest.teacher',$data);
   }  

   public function send_contact(\App\Http\Requests\ContactValidation $req)
   {
      Mail::to('info@edukto.com')->cc('azizov.emil@gmail.com')->send(new \App\Mail\SendMail($req));

     return back()->with('message','Sizin mesajınız göndərildi');
   }
}
