<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryDashboardController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }


    public function recursive_unset(&$array, $unwanted_key) {
        if(empty($array[$unwanted_key]))
        {
             unset($array[$unwanted_key]);
        }

       
        foreach ($array as &$value) {
            if (is_array($value)) {
                $this->recursive_unset($value, $unwanted_key);
            }
        }
    }


    public function generateArrayForParentId($in, $parent = NULL){
        
        $i=1;

        // $this->recursive_unset($in, 'parent_id');
 
      foreach ($in as $key => $value) {
        if(is_numeric($key)){
            $in = $value;
            // $this->recursive_unset($in, 'parent_id');
            $out[$key] = $this->generateArrayForParentId(array_except($in, ['parent_id']), $parent);
        }else{
          $out[$key]=$value;
          if($key=="id"){
            $out['paren_id']=$parent;
           
            $parent=$value;
          }elseif($key=="children"){
            $in = $value;
             //$this->recursive_unset($in, 'parent_id');
            $out[$key] = $this->generateArrayForParentId(array_except($in, ['parent_id']), $parent);
          }
        }
      }
      return $out;
    }





    private function updated_categories()
    {
        $categories=\App\Category::whereNull('parent_id')->orderBy('sort_order_number','ASC')->with(['children'=>function($query){

        $query->orderBy('sort_order_number','ASC');
       }])->get()->toArray();

       
       $this->recursive_unset($categories, 'children');

       return json_encode($categories);
    }
   

    public function singledimensional($array)
    {

        $result = [];
        $count = 1;
        $temp = [];
        array_walk_recursive($array, function($v, $k) use(&$result, &$count, &$temp) {
            $temp[$k] = $v;
            if($count++ % 12 == 0) {//here %12 is number of columns in DB table :) 
                $temp["sort_order_number"] = intval($count / 11);
               // print_r($temp['children']." ");
                $result[] = $temp;
                $temp = [];
            }
        });

       return $result;

    }

    public function add_category(Request $request)
    {
    
        $store_object=(object) array_last(json_decode($request->json_data,true));
        
     //   print_r($store_object);

     
        
        $category=new \App\Category;
        

        $category->text_az=$store_object->text_az;
        $category->text_en=$store_object->text_en;
        $category->text_ru=$store_object->text_ru;
        $category->slug=$store_object->slug;
        $category->target=$store_object->target;
        $category->icon=($store_object->icon=='')?'empty':$store_object->icon;
        $category->save();


         return $this->updated_categories();

        


    }


    public function save_sort_order(Request $req)
    {
            $store_array= (object) $this->singledimensional($this->generateArrayForParentId(json_decode($req->sort_json_data,true),NULL));

            print_r( $store_array);
            foreach($store_array as $object)
            {      

              // echo $object['paren_id'];
                $category=\App\Category::find($object['id']);
                $category->parent_id=$object['paren_id'];
                $category->sort_order_number=$object['sort_order_number'];
                $category->save();
            //    echo $object['paren_id'];
            }    


          return $this->updated_categories();
    }


    public function delete_category(Request $req)
    {
         return \App\Category::destroy($req->category_id);
         
    }

    public function update_category(Request $req)
    {   
        $update_category_data=(object )$this->make_assoc_array($req->update_data);
        $category=\App\Category::find($update_category_data->mnu_id);
        $category->text_az=$update_category_data->mnu_text_az;
        $category->text_en=$update_category_data->mnu_text_en;
        $category->text_ru=$update_category_data->mnu_text_ru;
        $category->slug=$update_category_data->mnu_slug;
        $category->icon=$update_category_data->mnu_icon;
        $category->target=$update_category_data->mnu_target;
        $category->save();
       


 

              
    }


    public function make_assoc_array($arr)
    {
        $assoc_array=array();

        foreach ($arr as $key => $value) {
            $assoc_array[$value['name']]=$value['value'];
                
        }

        return $assoc_array;
    }
}
