<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request, Image;
use Illuminate\Support\Facades\Storage;
use DB, Auth, Hash;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$categories=\App\Category::whereNull('parent_id')->with('children')->get()->toArray();

      //  dd($categories);
        return view('home');
    }

    public function category()
    {
        $categories=\App\Category::whereNull('parent_id')->orderBy('sort_order_number','ASC')->with(['children'=>function($query){

        $query->orderBy('sort_order_number','ASC');
       }])->get()->toArray();
       
       $this->recursive_unset($categories, 'children');

 
 
       $data['categories']=array_except($categories, ['parent_id']);
      
            return view('dashboard.category',$data);
    }

    public function recursive_unset(&$array, $unwanted_key) {
        if(empty($array[$unwanted_key]))
        {
             unset($array[$unwanted_key]);
        }

       
        foreach ($array as &$value) {
            if (is_array($value)) {
                $this->recursive_unset($value, $unwanted_key);
            }
        }
    }


    public function add_content()
    {

        $data =\App\Category::with('children')->whereNull('parent_id')->orderBy('sort_order_number','ASC')->get()->toArray();

      //dd($data);
      //  $recursiveArray =  $this->recursiveElements($data);

      $categories =$this->flattenDown($data);
      $kept_content_images=old('content_images');//when edit image this will be full
      $id='null';
 
/*$image = base64_decode($url);
$image_name= 'file_name.png';
$path = public_path() . "/" . $image_name;

file_put_contents($path, $image);*/


       
        return view('dashboard.add_content',compact('categories','kept_content_images','id'));
    }


    /*function printTree($tree, $r = 0, $p = null) {
          $elements = [];

    foreach ($tree as $i => $t) {
        $dash = ($t['parent_id'] == null) ? '' : str_repeat('-', $r) .' ';

        $elements[$t['id']]=$dash.$t['text_az'];

 
        if ($t['parent_id'] == $p) {
            // reset $r
            $r = 0;
        }
        if (isset($t['children'])) {
            $this->printTree($t['children'], $r++, $t['parent_id']);
        }
    }

    return $elements;
}*/



    public function flattenDown($data, $index=0) {
        $elements = [];
        foreach($data as $element) {
            
            $id=$element['id'];
            $elements[ $id] = str_repeat('&nbsp;&nbsp;&nbsp;', $index) . $element['text_az'];
            if(!empty($element['children'])) 
                {
                    $elements =$elements+$this->flattenDown($element['children'], $index+1);
                }
        }
        return $elements;
    }


    public function upload_content_images(\App\Http\Requests\Admin\ValidateImage $req)
    {
      //   $path = $req->file('file')->store('public/content_images');
      $file=$req->file('file');
      $extension = $file->getClientOriginalExtension();
      $renamed_file=uniqid('img_').'.'.$extension;

      $big_image_width=env('KEEP_CONTRAINT_BIG_IMAGE_WIDTH');
      $thumb_img_width=env('KEEP_CONTRAINT_THUMBNAIL_IMAGE_WIDTH');
      //$watermark_path=public_path('/uploads/watermark.png');
      $watermark_path=public_path('storage/content_images/watermark.png');
 
      $img = Image::make($req->file('file'))->resize($big_image_width, null, function ($constraint) {
        $constraint->aspectRatio();
        })->insert($watermark_path, 'center');
     

      Storage::put("public/content_images/big_images/".$renamed_file, $img->stream());


      $thumb_img = Image::make($req->file('file'))->resize($thumb_img_width, null, function ($constraint) {
        $constraint->aspectRatio();
        });


      Storage::put("public/content_images/thumbnail/".$renamed_file, $thumb_img->stream());


      //$content_image=new \App\Image;            
      return  $renamed_file;
             
    }


    public function delete_content_image(Request $req)
    {
      //return dd($req->file('file'));

      //echo $req->filename;
         Storage::delete('public/content_images/thumbnail/'.$req->filename);
         Storage::delete('public/content_images/big_images/'.$req->filename);

         if($req->id!=null)
         {
            $content=\App\Image::where('image',$req->filename);
            $content->delete();
         }
    } 

    public function store_content(\App\Http\Requests\Admin\ValidateContent $req)
    {

      $content=new \App\Content;
      $content->title_az=$req->title_az;
      $content->title_en=$req->title_en;
      $content->title_ru=$req->title_ru;
      $content->desc_az=$req->desc_az;
      $content->desc_en=$req->desc_en;
      $content->desc_ru=$req->desc_ru;
      $content->category_id=$req->category_id;

      $content->save();


$category=\App\Category::find($req->category_id);
$category->used=1;
$category->save();


      if(!empty($req->content_images))
      {
        $count=0;
          foreach ($req->content_images as $content_image) {

            $image=new \App\Image();

            $image->image=$content_image;
            $image->content_id=$content->id;
            $image->sort_order_number=$count++;
            $image->save();        
          }

       }

       return redirect('admin/contents')->with('message','Dəyişiklik edildi');
    }



    public function contents()
    {
      $contents=\App\Content::with(['images'=>function($query){

$query->orderBy('sort_order_number','ASC');

      }])->with('category')->paginate(5);
      return view('dashboard.contents',compact('contents'));
    }


    public function gallery()
    {
      $data['stored_images']=\App\Gallery::orderBy(DB::raw('ISNULL(order_number), order_number'), 'ASC')->get();
      return view('dashboard.gallery',$data);      
    }

    public function upload_gallery(\App\Http\Requests\Admin\ValidateGalleryImage $req)
    {
          //   $path = $req->file('file')->store('public/content_images');
      $file=$req->file('file');
      $extension = $file->getClientOriginalExtension();
      $renamed_file=uniqid('img_').'.'.$extension;

      $width=820;
 
      //$watermark_path=public_path('/uploads/watermark.png');
      $watermark_path=public_path('storage/gallery/watermark.png');
 
      $img = Image::make($req->file('file'))->resize($width, null, function ($constraint) {
        $constraint->aspectRatio();
        })->insert($watermark_path, 'center');
     

      Storage::put("public/gallery/".$renamed_file, $img->stream());

      $gallery_image=new \App\Gallery;
      $gallery_image->name= $renamed_file;
      $gallery_image->save();   
      return  $renamed_file;
    }


    public function rotate_gallery_image(Request $req)
    {
     // $degree=0;
      $img = Image::make(public_path('storage/'.$req->folder.'/'.$req->filename));

 //echo ''.public_path('storage/'.$req->folder.'/'.$req->filename);
      $img->rotate($req->degree);
      $img->save();
        return $req->degree;
    }


    public function rotate_content_image(Request $req)
    {


      $degree=$req->degree;
      $img = Image::make(public_path('storage/'.$req->folder.'_images/big_images/'.$req->filename));

 
      $img->rotate($degree);
      $img->save();



      $img_thumb = Image::make(public_path('storage/'.$req->folder.'_images/thumbnail/'.$req->filename));

 
      $img_thumb->rotate($degree);
      $img_thumb->save();




        return $req->filename;
    }

    public function gallery_images_sort(Request $req)
    {

     for ($i=0; $i <count($req->sort_array) ; $i++) { 

      \App\Gallery::where('name',$req->sort_array[$i])->update(['order_number' => $i]);
      

       
     }
        //print_r($req->sort_array);
    }
 public function content_images_sort(Request $req)
    {

     for ($i=0; $i <count($req->sort_array) ; $i++) { 

      \App\Image::where('image',$req->sort_array[$i])->update(['sort_order_number' => $i]);
      

       
     }
        //print_r($req->sort_array);
    }

    public function delete_gallery_image(Request $req)
    {


    \App\Gallery::where('name', $req->filename)->delete();
   Storage::disk('public')->delete('gallery/'.$req->filename);
 

  // Storage::delete('file.jpg')disk('public')->delete('gallery/'.$req->filename);
      
    }


    public function edit_content($id)
    {
      $data['kept_content_images']=\App\Image::where('content_id',$id)->orderBy('sort_order_number','ASC')->pluck('image')->toArray();
      $categories =\App\Category::with('children')->whereNull('parent_id')->orderBy('sort_order_number','ASC')->get()->toArray();
      $data['id']=$id;
      $data['content']=\App\Content::find($id);
      //dd($data);
      //  $recursiveArray =  $this->recursiveElements($data);

      $data['categories'] =$this->flattenDown($categories);
      return view('dashboard.edit_content',$data);
    }


    public function upload_more_content_images(\App\Http\Requests\Admin\ValidateImage $req,$id)
    {
        $file=$req->file('file');
      $extension = $file->getClientOriginalExtension();
      $renamed_file=uniqid('img_').'.'.$extension;

      $big_image_width=env('KEEP_CONTRAINT_BIG_IMAGE_WIDTH');
      $thumb_img_width=env('KEEP_CONTRAINT_THUMBNAIL_IMAGE_WIDTH');
      //$watermark_path=public_path('/uploads/watermark.png');
      $watermark_path=public_path('storage/content_images/watermark.png');
 
      $img = Image::make($req->file('file'))->resize($big_image_width, null, function ($constraint) {
        $constraint->aspectRatio();
        })->insert($watermark_path, 'center');
     

      Storage::put("public/content_images/big_images/".$renamed_file, $img->stream());


      $thumb_img = Image::make($req->file('file'))->resize($thumb_img_width, null, function ($constraint) {
        $constraint->aspectRatio();
        });


      Storage::put("public/content_images/thumbnail/".$renamed_file, $thumb_img->stream());


      $content_image=new \App\Image;
      $content_image->image= $renamed_file;
      $content_image->content_id= $id;
      $content_image->save(); 

      //$content_image=new \App\Image;            
      return  $renamed_file;
    }


    public function save_edit_content(\App\Http\Requests\Admin\ValidateContent $req,$id)
    {
      $content=\App\Content::find($id);
      $content->category_id=$req->category_id;
      $content->title_az=$req->title_az;
      $content->title_en=$req->title_en;
      $content->title_ru=$req->title_ru;
      $content->desc_az=$req->desc_az;
      $content->desc_en=$req->desc_en;
      $content->desc_ru=$req->desc_ru;
      $content->save();


      return redirect('admin/contents')->with('message','Dəyişiklik edildi');
    
    }


    public function delete_content($id)
    {
       
        $related_images=\App\ContentImage::select('image')->where('content_id',$id)->get();
        foreach ($related_images as $related_image) {
 
            Storage::disk('public')->delete('content_images/big_images/'.$related_image->image);
            Storage::disk('public')->delete('content_images/thumbnail/'.$related_image->image);
        }



           $category_id=\App\Content::find($id)->category_id;
           //make available cat
           $category=\App\Category::find($category_id);
           $category->used=0;
           $category->save();
         //  echo $category_id;

           \App\Content::destroy($id);
          return redirect('admin/contents');
    }


    public function teachers(){

      $data['teachers']=\App\Teacher::orderBy('id','DESC')->paginate('6');
      return view('dashboard.teachers',$data);
    }


    public function add_teacher()
    {
      return view('dashboard.add_teacher');
    }


    public function store_teacher(\App\Http\Requests\Admin\ValidateTeacher $req,$edit_id=null)
    {
         

         if(!is_null($edit_id) && isset($edit_id))//if edititon
              {

               $teacher= \App\Teacher::find($edit_id);
             }
             else{
                  $teacher=new \App\Teacher;
            }


    $teacher->course_az=$req->course_az;
    $teacher->course_en=$req->course_en;
    $teacher->course_ru=$req->course_ru;
    $teacher->desc_az=$req->desc_az;
    $teacher->desc_en=$req->desc_en;
    $teacher->desc_ru=$req->desc_ru;
    $teacher->fullname_az=$req->fullname_az;
    $teacher->fullname_en=$req->fullname_en;
    $teacher->fullname_ru=$req->fullname_ru;

/* move image start*/
    $png_url = uniqid().'.png';
    $path = public_path().'/storage/team/' . $png_url;

    Image::make(file_get_contents($req->image_cropped_base64))->save($path); 
/* move image end */
    $teacher->image=$png_url;
    $teacher->save();

    return redirect('admin/teachers')->with('message','Yadda saxlanildi');
    }


    public function delete_teacher($id)
    {
         


        $teacher=\App\Teacher::find($id);
        

        Storage::disk('public')->delete('team/'.$teacher->image);
              \App\Teacher::where('id', $id)->delete();
        return back();
    }


    public function edit_teacher($id)
    {
$teacher = \App\Teacher::find($id);
 $data['edit_boolean']=true;
 $data['id']=$id;


 return view('dashboard.edit_teacher',$data)->with('teacher', $teacher);
    }



    public function change_password()
    {
      return view('dashboard.change_password');
    }


    public function store_password(\App\Http\Requests\Admin\ValidatePassword $request)
    {
          $user_id=Auth::user()->id;
          $user= \App\User::find($user_id);

          $user->password=Hash::make($request->password);     
          $user->save();

          return back()->with('message', 'Dəyişiklikləriniz yadda saxlanıldı!');
    }
}

