<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ValidateContent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'title_az'=>'required',
           'title_en'=>'required',
           'title_ru'=>'required',
           'desc_az'=>'required',
           'desc_en'=>'required',
           'desc_ru'=>'required',
           'category_id'=>'required',
           //'content_images'=>'required'
        ];
    }
}
