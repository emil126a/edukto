<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ValidateTeacher extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo'=>'image',
            'image_cropped_base64'=>'required',
            'course_az'=>'required',
            'course_en'=>'required',
            'course_ru'=>'required',
            'fullname_az'=>'required',
            'fullname_en'=>'required',
            'fullname_ru'=>'required',
        ];
    }
}
