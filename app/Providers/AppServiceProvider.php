<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider, View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $categories=\App\Category::whereNull('parent_id')->orderBy('sort_order_number','ASC')->with(['children'=>function($query){

        $query->orderBy('sort_order_number','ASC');
       }])->get()->toArray();

       View::share('categories',$categories);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
