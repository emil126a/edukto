<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{



   public function images()
   {
   		return $this->hasMany('App\Image');
   }	


   public function category()
   {
   		return $this->hasOne('App\Category','id','category_id');
   }
}

