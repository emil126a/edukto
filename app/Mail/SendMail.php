<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;


class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    private $request;

    public function __construct(Request $req)
    {
       $this->request=$req;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@edukto.com',$this->request->name)
                    ->replyTo($this->request->email, $this->request->fullname)
                    ->subject('Sizin yenə ismarıcınız var')
                    ->markdown('emails.send_mail')
                    ->with([
                        'message' => $this->request->message,                     
                        'ip' => $this->request->ip(),
                        'email'=>$this->request->email                        
                            ]);
        // return $this->view('view.name');
    }
}
