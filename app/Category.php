<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $guarded = ['id'];
 	public function parent()
 	{
 		return $this->belongsTo('App\Category','parent_id')->where('parent_id',NULL)->with('parent');;
 	}

 	public function children()
 	{
 		return $this->hasMany('App\Category','parent_id')->with('children');
 	}


   public function content()
   {
   	   return $this->belongsTo('App\Content','id','category_id');
   }
}
