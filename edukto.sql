/*
 Navicat Premium Data Transfer

 Source Server         : homestead
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : 192.168.10.10:3306
 Source Schema         : edukto

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 08/02/2018 16:38:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `text_az` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_ru` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_order_number` int(11) DEFAULT NULL,
  `parent_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp(0) DEFAULT NULL,
  `updated_at` timestamp(0) DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `used` tinyint(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_categories_slug`(`slug`) USING BTREE,
  INDEX `fk_categories_parent_id`(`parent_id`) USING BTREE,
  CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 112 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (97, 'Müəllimlər', 'Teachers', 'Müəllimlər', 'teachers', 2, NULL, '2017-12-20 13:38:09', '2018-02-08 11:53:32', 'fa fa-bar-chart-o', '_self', 1);
INSERT INTO `categories` VALUES (98, 'Homepage', 'Homepage', 'Homepage', 'homepage', 1, NULL, '2017-12-25 08:05:44', '2018-01-12 07:03:03', 'fa fa-home', '_self', 1);
INSERT INTO `categories` VALUES (104, 'Xidmətlər', 'Services', 'Xidmətlər', 'services', 4, NULL, '2017-12-26 06:05:06', '2018-01-17 08:12:22', 'fa fa-sliders', '_blank', 0);
INSERT INTO `categories` VALUES (105, 'Əlaqə', 'Contact', 'Əlaqə', 'contact', 8, NULL, '2018-01-10 11:31:37', '2018-01-17 08:12:53', 'fa fa-phone', '_self', 1);
INSERT INTO `categories` VALUES (106, 'Tərcümə xidmətləri', 'Translation services', 'Tərcümə xidmətləri', 'translation_services', 6, 104, '2018-01-10 11:32:17', '2018-01-17 08:12:42', 'fa fa-bar-chart-o', '_self', 0);
INSERT INTO `categories` VALUES (107, 'Sınaq imtahanları', 'Trial exams', 'Sınaq imtahanları', '#', 5, 104, '2018-01-11 07:48:04', '2018-01-17 08:12:33', 'fa fa-asterisk', '_self', 0);
INSERT INTO `categories` VALUES (108, 'Qalereya', 'Gallery', 'Qalereya', 'gallery', 7, NULL, '2018-01-11 08:29:27', '2018-01-17 08:10:46', 'fa fa-picture-o', '_self', 1);
INSERT INTO `categories` VALUES (109, 'Haqqımızda', 'About', 'Haqqımızda', 'about', 3, NULL, '2018-01-15 12:22:45', '2018-01-17 08:12:03', 'fa fa-question-circle', '_self', 0);
INSERT INTO `categories` VALUES (111, 'dadasd', 'adsasd', 'ada', 'asdasd', NULL, NULL, '2018-02-08 12:20:17', '2018-02-08 12:20:17', 'empty', '_blank', 0);

-- ----------------------------
-- Table structure for content_images
-- ----------------------------
DROP TABLE IF EXISTS `content_images`;
CREATE TABLE `content_images`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) DEFAULT NULL,
  `updated_at` timestamp(0) DEFAULT NULL,
  `sort_order_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `content_images_content_id_foreign`(`content_id`) USING BTREE,
  CONSTRAINT `content_images_ibfk_1` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of content_images
-- ----------------------------
INSERT INTO `content_images` VALUES (17, 'img_5a7ab56507123.jpg', 15, '2018-02-07 08:15:27', '2018-02-08 08:30:04', 0);
INSERT INTO `content_images` VALUES (23, 'img_5a7ab566b0e26.jpg', 15, '2018-02-07 08:15:27', '2018-02-08 08:30:04', 3);
INSERT INTO `content_images` VALUES (24, 'img_5a7ab56766713.jpg', 15, '2018-02-07 08:15:27', '2018-02-08 08:30:04', 1);
INSERT INTO `content_images` VALUES (31, 'img_5a7acf74de766.jpg', 15, '2018-02-07 10:05:41', '2018-02-08 08:30:04', 2);
INSERT INTO `content_images` VALUES (34, 'img_5a7bf82527268.jpg', 17, '2018-02-08 07:11:42', '2018-02-08 07:11:54', 1);
INSERT INTO `content_images` VALUES (35, 'img_5a7bf82527447.jpg', 17, '2018-02-08 07:11:42', '2018-02-08 07:11:54', 0);

-- ----------------------------
-- Table structure for contents
-- ----------------------------
DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title_az` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ru` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_az` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_en` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_ru` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) DEFAULT NULL,
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `contents_category_id_unique`(`category_id`) USING BTREE,
  CONSTRAINT `contents_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contents
-- ----------------------------
INSERT INTO `contents` VALUES (15, 'Xidmetler haqqinda', 'Xidmetler haqqinda', 'Xidmetler haqqinda', '<p>wwqeqwasdad asdadas dad</p>', '<p>qweeqe</p>', '<p>qweqeewq</p>', 104, '2018-02-07 08:15:27', '2018-02-07 10:29:52');
INSERT INTO `contents` VALUES (17, 'asdada', 'asdadas', 'adsadad', '<p>asdada</p>', '<p>asdadada</p>', '<p>adada</p>', 109, '2018-02-08 07:11:42', '2018-02-08 07:11:42');

-- ----------------------------
-- Table structure for gallery
-- ----------------------------
DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `order_number` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 189 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gallery
-- ----------------------------
INSERT INTO `gallery` VALUES (121, 'img_5a746379ce12e.jpg', '2018-02-02 13:11:22', '2018-02-08 11:51:36', 2);
INSERT INTO `gallery` VALUES (122, 'img_5a746379ce227.jpg', '2018-02-02 13:11:22', '2018-02-08 11:51:36', 8);
INSERT INTO `gallery` VALUES (123, 'img_5a74637a57661.jpg', '2018-02-02 13:11:22', '2018-02-08 11:51:36', 1);
INSERT INTO `gallery` VALUES (124, 'img_5a74637a9f795.jpg', '2018-02-02 13:11:22', '2018-02-08 11:51:36', 7);
INSERT INTO `gallery` VALUES (125, 'img_5a74637ad7af1.jpg', '2018-02-02 13:11:23', '2018-02-08 11:51:36', 9);
INSERT INTO `gallery` VALUES (126, 'img_5a74637b1dcd2.jpg', '2018-02-02 13:11:23', '2018-02-08 11:51:36', 0);
INSERT INTO `gallery` VALUES (128, 'img_5a74637c24431.jpg', '2018-02-02 13:11:24', '2018-02-08 11:51:36', 3);
INSERT INTO `gallery` VALUES (130, 'img_5a746e012de0d.jpg', '2018-02-02 13:56:17', '2018-02-08 11:51:36', 5);
INSERT INTO `gallery` VALUES (187, 'img_5a79688d868cc.jpg', '2018-02-06 08:34:21', '2018-02-08 11:51:36', 6);
INSERT INTO `gallery` VALUES (188, 'img_5a7a92137f3a0.jpg', '2018-02-07 05:43:48', '2018-02-08 11:51:36', 4);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2017_11_03_140344_create_categories', 1);
INSERT INTO `migrations` VALUES (4, '2017_11_03_140507_create_contents', 1);
INSERT INTO `migrations` VALUES (5, '2017_11_03_140542_create_content_images', 1);
INSERT INTO `migrations` VALUES (7, '2017_11_13_150902_modify_category', 2);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for teachers
-- ----------------------------
DROP TABLE IF EXISTS `teachers`;
CREATE TABLE `teachers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_az` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `course_en` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `course_ru` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `desc_az` longtext CHARACTER SET utf8 COLLATE utf8_general_ci,
  `desc_en` longtext CHARACTER SET utf8 COLLATE utf8_general_ci,
  `desc_ru` longtext CHARACTER SET utf8 COLLATE utf8_general_ci,
  `fullname_az` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fullname_en` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fullname_ru` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teachers
-- ----------------------------
INSERT INTO `teachers` VALUES (1, 'adasd', 'wwer', 'ewr', NULL, NULL, NULL, 'Emil Azizov', 'Emil Azizov', 'Emil Azizov', '1.jpg');
INSERT INTO `teachers` VALUES (2, NULL, NULL, NULL, NULL, NULL, NULL, 'Ziya Gafarli', 'Ziya Gafarli', 'Ziya Gafarli', '2.jpg');
INSERT INTO `teachers` VALUES (3, NULL, NULL, NULL, NULL, NULL, 'dsadasd', 'Samrad Ismayilov', 'Samrad Ismayilov', 'Samrad Ismayilov', '3.jpg');
INSERT INTO `teachers` VALUES (4, 'dfs', NULL, NULL, NULL, NULL, NULL, 'Sanan Yusifov', 'Sanan Yusifov', 'Sanan Yusifov', '4.jpg');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp(0) DEFAULT NULL,
  `updated_at` timestamp(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Emil Azizov', 'azizov.emil@gmail.com', '$2y$10$6rIanQuCiWKrq.s5BzirXOpg6H494R.lkn.3iMrH/4917ADH.Juiq', 'H7p3N4uOmkio6atB0OeNby1fucmlNcdzvHmfmHH5i6VjKZF0m58xyaSnWKzF', '2017-11-13 06:15:18', '2017-11-13 06:15:18');

SET FOREIGN_KEY_CHECKS = 1;
