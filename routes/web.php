<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(
[
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localize' ] // Route translate middleware
],
function() {
	 
	 Route::get('/', 'GuestController@homepage');

 



Auth::routes();
Route::get('admin/home', 'DashboardController@index')->name('home');
Route::get('admin/category', 'DashboardController@category');
Route::get('admin/add_content', 'DashboardController@add_content');
Route::post('admin/add_content', 'DashboardController@store_content');
Route::get('admin/delete_teacher/{id}', 'DashboardController@delete_teacher');

Route::get('admin/edit_content/{id}', 'DashboardController@edit_content');

Route::get('admin/delete_content/{id}', 'DashboardController@delete_content');



Route::post('admin/edit_content/{id}', 'DashboardController@save_edit_content');

Route::post('admin/upload_content_images', 'DashboardController@upload_content_images');

Route::post('delete_content_image', 'DashboardController@delete_content_image');

Route::get('admin/gallery', 'DashboardController@gallery');
Route::post('admin/upload_gallery', 'DashboardController@upload_gallery');

Route::post('/add_category', 'CategoryDashboardController@add_category');
Route::post('/save_order','CategoryDashboardController@save_sort_order');
Route::post('/delete_category','CategoryDashboardController@delete_category');
Route::post('/update_category','CategoryDashboardController@update_category');	 

Route::post('rotate_gallery_image', 'DashboardController@rotate_gallery_image');

Route::get('admin/edit_teacher/{id}', 'DashboardController@edit_teacher');
Route::post('admin/edit_teacher/{id}', 'DashboardController@store_teacher');


Route::post('rotate_content_image', 'DashboardController@rotate_content_image');






Route::post('gallery_images_sort', 'DashboardController@gallery_images_sort');

Route::post('content_images_sort', 'DashboardController@content_images_sort');


Route::post('admin/upload_more_content_images/{id}', 'DashboardController@upload_more_content_images');

Route::get('admin/teachers', 'DashboardController@teachers');
Route::get('admin/add_teacher', 'DashboardController@add_teacher');
Route::post('admin/add_teacher', 'DashboardController@store_teacher');



Route::post('delete_gallery_image', 'DashboardController@delete_gallery_image');



Route::get('admin/contents', 'DashboardController@contents');
Route::get('admin/change_password', 'DashboardController@change_password');
Route::post('admin/change_password', 'DashboardController@store_password');
/*----------------------------*/


Route::get('/{slug}', 'GuestController@show');
Route::post('/contact', 'GuestController@send_contact');
Route::get('/teacher/{id}', 'GuestController@teacher');


});

